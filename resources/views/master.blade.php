<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no,maximum-scale=1,user-scalable=no">
    <title>Moving Professionals 4U</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;Raleway:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- owl-carousel -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}" />
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" />
    <!-- media element player -->
    <link href="{{ asset('css/mediaelementplayer.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Animate -->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
    <!-- Style -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- Responsive -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <!-- custom style -->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}" />

    <!-- Custom JS -->

    <!-- jquery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- popper -->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- main -->
    <script src="{{ asset('js/main.js') }}"></script>
    <!-- google captcha code -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- custom -->
    <script src="{{ asset('js/custom.js') }}"></script>


</head>
<body data-spy="scroll" data-offset="80">
    <!-- loading -->
    <div id="loading">
        <div id="loading-center">
            <div class="loader">
                <div class="cube">
                    <div class="sides">
                        <div class="top"></div>
                        <div class="right"></div>
                        <div class="bottom"></div>
                        <div class="left"></div>
                        <div class="front"></div>
                        <div class="back"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- loading End -->

    @yield("content")

    <!-- Footer -->
    <br/><br/>
    <footer>
        <!-- Subscribe Our Newsletter -->
        <section class="iq-ptb-80 iq-newsletter iq-bg-over iq-over-blue-90 jarallax" data-jarallax-video="m4v:./video/01.m4v,webm:./video/01.webm,ogv:./video/01.ogv" style="margin-top: -53px;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="heading-title white iq-mb-40">
                            <h3 class="title iq-tw-7">Subscribe Our Newsletter</h3>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-7">
                        <form class="form-inline">
                            <div class="form-group">
                                <label for="email" class="sr-only">Email</label>
                                <input type="text" class="form-control" id="email" placeholder="Email">
                            </div>
                            <a class="button bt-white iq-ml-25" href="javascript:void(0)">Subscribe</a>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- Subscribe Our Newsletter END -->
        <!-- Footer Info -->
        @if($_SERVER['REQUEST_URI'] == '/')
        <section id="contact-us" class="footer-info white-bg">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-md-6 col-lg-4">
                        <div class="iq-get-in">
                            <h4 class="iq-tw-7 iq-mb-20">Get in Touch</h4>
                            <form id="contact" method="post">
                                <div class="contact-form">
                                    <div class="section-field">
                                        <input class="require" id="contact_name" type="text" placeholder="Name*" name="name">
                                    </div>
                                    <div class="section-field">
                                        <input class="require" id="contact_email" type="email" placeholder="Email*" name="email">
                                    </div>
                                    <div class="section-field">
                                        <input class="require" id="contact_phone" type="text" placeholder="Phone*" name="phone">
                                    </div>
                                    <div class="section-field textarea">
                                        <textarea id="contact_message" class="input-message require" placeholder="Comment*" rows="5" name="message"></textarea>
                                    </div>
                                    <button id="submit" name="submit" type="submit" value="Send" class="button iq-mt-15">Send Message</button>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert" id="success">
                                        <strong>Thank You, Your message has been received.</strong>.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.840108181602!2d144.95373631539215!3d-37.8172139797516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1497005461921" style="border:0" allowfullscreen></iframe>
        </section>
        @endif
        <section class="footer-info iq-pt-60">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <div class="iq-footer-box text-left">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-location-outline"></i>
                            </div>
                            <div class="footer-content">
                                <h4 class="iq-tw-6 iq-pb-10">Address</h4>
                                <p>1234 North Avenue Luke Lane, South Bend, IN 360001</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 r4-mt-30">
                        <div class="iq-footer-box text-left">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-telephone-outline"></i>
                            </div>
                            <div class="footer-content">
                                <h4 class="iq-tw-6 iq-pb-10">Phone</h4>
                                <p>+0123 456 789
                                    <br>Mon-Fri 8:00am - 8:00pm
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 r-mt-30">
                        <div class="iq-footer-box text-left">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-email-outline"></i>
                            </div>
                            <div class="footer-content">
                                <h4 class="iq-tw-6 iq-pb-10">Mail</h4>
                                <p>support@sofbox.com
                                    <br>24 X 7 online support
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 align-self-center">
                        <ul class="info-share">
                            <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-google"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="row iq-mt-40">
                    <div class="col-sm-12 text-center">
                        <div class="footer-copyright iq-ptb-20">Copyright @<span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script></span> <a href="javascript:void(0)" class="text-green">Moving Professionals.</a> All Rights Reserved </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer Info END -->
    </footer>
    <!-- Footer END -->

    <!-- back-to-top -->
    <div id="back-to-top">
        <a class="top" id="top" href="#top"> <i class="ion-ios-upload-outline"></i> </a>
    </div>
    <!-- back-to-top End -->

</body>

</html>
