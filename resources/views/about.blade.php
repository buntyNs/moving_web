@extends("master")

@section("content")
    <!-- Header -->
    <header id="main-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{url('/')}}">
                            <img class="img-fluid" src="{{ asset('images/logo.png') }}" alt="#">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="ion-navicon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto w-100 justify-content-end">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/')}}">Home</a>
                                </li>
                                {{--<li class="nav-item">
                                    <a class="nav-link" href="{{url('/services')}}">Services</a>
                                </li>--}}
                                <li class="nav-item active">
                                    <a class="nav-link active" href="{{url('/about-us')}}">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/contact-us')}}">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->
    <!-- Banner -->
    <div class="main-content">
        <section id="iq-home" class="iq-banner overview-block-pt iq-bg-over iq-over-blue-90 iq-parallax" data-jarallax='{"speed": 0.6}'>
            <div class="container-fluid">
                <div class="banner-text">
                    <div class="row">
                        <div class="col-lg-6">
                            <h1 class="text-uppercase iq-font-white iq-tw-3"><b class="iq-tw-7">Locating in the heart of Henderson, NV., </b>Moving Professionals</h1>
                        </div>
                        <div class="col-lg-6">
                            <div class="row"></div>
                            <div class="row"><img class="banner-img" src="{{asset('images/moving_goods_serv.jpg')}}" alt=""></div>
                            <div class="row"></div>
                        </div>
                    </div>
                </div><br/><br/>
            </div>
            <div class="banner-objects">
                <span class="banner-objects-01" data-bottom="transform:translatey(50px)" data-top="transform:translatey(-50px);">
                        <img src="images/drive/03.png" alt="drive02">
                    </span>
                <span class="banner-objects-02 iq-fadebounce">
                        <span class="iq-round"></span>
                </span>
                <span class="banner-objects-03 iq-fadebounce">
                        <span class="iq-round"></span>
                </span>
            </div>
        </section>
        <!-- Banner End -->
        <section id="how-works" class="overview-block-ptb how-works">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left">
                            <h4>Caring, professional Staff</h4><br/>
                            <p>Moving Professionals 4U is a nicer way to move. Simple as that. We know how stressful moving day can be - but it doesn’t have to be. Every one of our employees is highly trained and cares about the happiness of our customers. From our movers in NYC to our operations team to our sales staff, we have the finest people in the moving and storage business, making us one of the best local moving companies, long distance moving companies, and office moving companies. Experience the difference that years of service and years of perfecting the personalized touch can mean for you and your family.</p>
                        </div>
                        <div class="col-md-6 pull-right">
                            <img class="banner-img about-img" width="500px" src="{{asset('images/staff.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="iq-objects">
                <span class="iq-objects-01">
                        <img src="images/drive/02.png" alt="drive02">
                    </span>
                <span class="iq-objects-02" data-bottom="transform:translatey(50px)" data-top="transform:translatey(-100px);">
                        <img src="images/drive/03.png" alt="drive02">
                    </span>
                <span class="iq-objects-03" data-bottom="transform:translatex(50px)" data-top="transform:translatex(-100px);">
                        <img src="images/drive/04.png" alt="drive02">
                    </span>
                <span class="iq-objects-04 iq-fadebounce">
                        <span class="iq-round"></span>
                </span>
            </div>
        </section>
        <section id="software-features" class="overview-block-ptb iq-mt-50 software">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left">
                            <img class="banner-img about-img" width="500px" src="{{asset('images/delivery.png')}}" alt="">
                        </div>
                        <div class="col-md-6 pull-right">
                            <h4>Timely Delivery</h4><br/>
                            <p>Moving Professionals 4U respects your time. Our Moving Professionals 4U movers will show up on the time provided / requested. We deliver on time, it’s as simple as that. We believe we are only as good as our word. So if we say we will be at your home or office ready to move between 9am and 11am on Monday, then that’s when we will arrive. Our movers in Moving Professionals 4U treat our customers the way we would like to be treated. Iif you’re not satisfied with our service, please let us know.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="iq-objects-software">
                <span class="iq-objects-01" data-bottom="transform:translatey(50px)" data-top="transform:translatey(-100px);">
                        <img src="images/drive/03.png" alt="drive02">
                    </span>
                <span class="iq-objects-02" data-bottom="transform:translatex(50px)" data-top="transform:translatex(-100px);">
                        <img src="images/drive/04.png" alt="drive02">
                    </span>
                <span class="iq-objects-03 iq-fadebounce">
                        <span class="iq-round"></span>
                </span>
            </div>
        </section>
        <section class="overview-block-ptb grey-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left">
                            <h4>Satisfaction Guaranteed</h4><br/>
                            <p>Moving Professionals 4U guarantees your satisfaction with every move and we put it in writing! The Moving Professionals 4U guarantee gives you peace of mind that comes from knowing that we have a full customer service department that will not leave any issue that could arise, unresolved. Our dedication to customer service has earned Moving Professionals 4U an A rating from The Better Business Bureau.</p>
                            <p>One of the most intimidating tasks required when moving a home or office space is the actual physical packing of placing items in boxes or crating furniture to be moved. Determining the safest way to pack and unpack your belongings is a hassle that requires a great deal of time and attention.</p>
                            <p>At Moving Professionals 4U, we are committed to making your moving process as smooth as possible. We offer a large variety of packing services to choose from, available for local and long distance. Our teams of expert movers and packers are highly skilled in preparing and packing even the most delicate of items, so you know your possessions will be fully protected throughout the duration of your move.</p>
                        </div>
                        <div class="col-md-6 pull-right">
                            <img class="banner-img about-img" width="500px" src="{{asset('images/guarantee.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="overview-block-ptb grey-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left">
                            <img class="banner-img about-img" width="500px" src="{{asset('images/packing.png')}}" alt="">
                        </div>
                        <div class="col-md-6 pull-right" style="margin-top: 50px;">
                            <h4>Packing Services</h4><br/>
                            <p>At Moving Professionals 4U, we understand that you may not have time to pack for your move. Work and family obligations make it difficult to dedicate the time to properly prepare and pack your items. With our assistance from our packing teams, we can take the hassle out of the packing process. Once you contact a Moving Professionals 4U Moving Manager, we will schedule an appointment to visit your home or office to determine the packing needs of your move.</p>
                            <p>You can choose to have your entire home or office professionally packed by our team, or select specific rooms you would like packed. Once we have all the necessary information, we are ready to pack! Our team will securely shrink wrap, blanket, box, and/or crate your items to prevent damage during the moving process.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="overview-block-ptb iq-bg-over  iq-over-blue-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left">
                            <h4>Interstate Moving begins with a plan.</h4><br/>
                            <p>Did you know that interstate moving, or a long distance move requires quite a bit of logistic planning. While it definitely may appear like a big project to move, we make it simple. It’s simple to us because we know what we are doing.. Starting with the quote process, our long distance Moving Managers work  with you to gather all the information needed to make a decision in addition to what is required to complete your long distance move on budget and on time. In acquiring this information, our long distance Moving Managers can help answer any question you may have, as well as providing a binding estimated cost. To make things a little easier for you, you can start the process by requesting a free long distance moving quote from Moving Professionals 4U, right here within our website.</p>
                        </div>
                        <div class="col-md-6 pull-right">
                            <img class="banner-img about-img" width="500px" src="{{asset('images/planning.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="overview-block-ptb iq-bg-over  iq-over-blue-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left">
                            <img class="banner-img about-img" width="500px" src="{{asset('images/airplane.jpg')}}" alt="">
                        </div>
                        <div class="col-md-6 pull-right">
                            <h4>We offer long distance storage options to keep your things safe until you need them.</h4><br/>
                            <p>Planning a long distance move? Moving Professionals 4U has you covered! One of the main things people tend to worry about is where they can store their items during a move. This is why Moving Professionals 4U offers long distance storage, short term as well as long term. Your items are completely safe with us.  Our storage, short term and long term has NO PUBLIC ACCESS.  Our long distance moving facilities are specifically designed to accommodate storage for every kind of need. Whether you have a small amount of storage to be had or need to store an entire home, we have the facilities to take care of this. If you want more information about our storage service, give us a call and we can customer taylor any kind of storage need.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="how-works" class="overview-block-ptb how-works">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left">
                            <h4>Office movers in Moving Professionals 4U – You take care of the business, we will take care of the move.</h4><br/>
                            <p>You have so many responsibilities at work, from managing employees, to servicing clients, to coming up with the next great plan of action. Let our office movers take you to your new home so you can stay focused on your business. Moving Professionals 4U is a professional and experienced moving service that has been doing this for years! And it’s no surprise that we are referred to as one of the best moving companies of Moving Professionals 4U City. Our affordable and experienced moving experience will show you why people never go back to moving on their own. Additionally, our moving expertise allows you to focus on your current business without missing a beat. Whether you’re moving your business along with your home or you’re expanding your business to another location, Moving Professionals 4U is the premier commercial moving company for companies of all sizes.</p>
                        </div>
                        <div class="col-md-6 pull-right">
                            <img class="banner-img about-img" width="500px" src="{{asset('images/office.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="iq-objects">
                <span class="iq-objects-01">
                        <img src="images/drive/02.png" alt="drive02">
                    </span>
                <span class="iq-objects-02" data-bottom="transform:translatey(50px)" data-top="transform:translatey(-100px);">
                        <img src="images/drive/03.png" alt="drive02">
                    </span>
                <span class="iq-objects-03" data-bottom="transform:translatex(50px)" data-top="transform:translatex(-100px);">
                        <img src="images/drive/04.png" alt="drive02">
                    </span>
                <span class="iq-objects-04 iq-fadebounce">
                        <span class="iq-round"></span>
                </span>
            </div>
        </section>
        <section id="software-features" class="overview-block-ptb iq-mt-50 software">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left">
                            <img class="banner-img about-img" width="500px" src="{{asset('images/organization.jpg')}}" alt="">
                        </div>
                        <div class="col-md-6 pull-right">
                            <h4>Moving Professionals 4U will make your COMMERCIAL moving experience organized and stress free.</h4><br/>
                            <p>Do you know what makes Moving Professionals 4U is one of the best commercial movers? With our years of experience has allowed us to be one of the leading commercial moving companies in the area. Our history has made us the experts when it comes to your commercial move. During your commercial move, transporting your office’s furniture and files is not a problem for us— our strategic system of packaging, transporting and unloading of your business’s items, ensures safety, efficiency, and speed of transfer. We understand every aspect of commercial moving which lets us avoid unnecessary expense that so many inexperienced movers can incur for their customers.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="iq-objects-software">
                <span class="iq-objects-01" data-bottom="transform:translatey(50px)" data-top="transform:translatey(-100px);">
                        <img src="images/drive/03.png" alt="drive02">
                    </span>
                <span class="iq-objects-02" data-bottom="transform:translatex(50px)" data-top="transform:translatex(-100px);">
                        <img src="images/drive/04.png" alt="drive02">
                    </span>
                <span class="iq-objects-03 iq-fadebounce">
                        <span class="iq-round"></span>
                </span>
            </div>
        </section>
    </div>
@endsection