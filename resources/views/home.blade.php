@extends("master")

@section("content")
    <!-- Header -->
    <header id="main-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{url('/')}}">
                            <img class="img-fluid" src="{{ asset('images/logo.png') }}" alt="#">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="ion-navicon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto w-100 justify-content-end">
                                <li class="nav-item active">
                                    <a class="nav-link active" href="{{url('/')}}">Home</a>
                                </li>
                                {{--<li class="nav-item">
                                    <a class="nav-link" href="{{url('/services')}}">Services</a>
                                </li>--}}
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/about-us')}}">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/contact-us')}}">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->
    <!-- Banner -->
    <div class="main-content">
        <section id="iq-home" class="iq-banner overview-block-pt iq-bg-over iq-over-blue-90 iq-parallax" data-jarallax='{"speed": 0.6}'>
            <div class="container-fluid">
                <div class="banner-text">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <h1 style="font-size: 50px;" class="text-uppercase iq-font-white iq-tw-3"><b class="iq-tw-7">Moving Professionals 4U, LLC</b> is a family owned and operated business with over 15 years of experience!</h1>
                            <p style="font-size: 16px;" class="iq-font-white iq-pt-15 iq-mb-40">Call to today to speak with one of our Moving Managers about your local or long distance relocation. We move everything from one or two items, to apartments, large homes, small and large offices as well.  We provide free, in home estimates to discuss specific services and cost evaluation.</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="row"></div>
                            <div class="row"><img class="banner-img home-banners" style="padding-top: 40px;" src="{{asset('images/move_homes.jpg')}}" alt=""></div>
                            <div class="row"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-objects">
                <span class="banner-objects-01" data-bottom="transform:translatey(50px)" data-top="transform:translatey(-50px);">
                        <img src="images/drive/03.png" alt="drive02">
                    </span>
                <span class="banner-objects-02 iq-fadebounce">
                        <span class="iq-round"></span>
                </span>
                <span class="banner-objects-03 iq-fadebounce">
                        <span class="iq-round"></span>
                </span>
            </div>
        </section>
        <!-- Banner End -->
        <section id="how-works" class="overview-block-ptb how-works" style="margin-bottom: -100px">
            <div class="container">
                <div class="row res-move">
                    <div class="col-md-12">
                        <div class="col-md-8 pull-left">
                            <div class="heading-title" style="margin-left: -110px;">
                                <h3 class="title iq-tw-7">Residential Moving</h3>
                                <p style="opacity: 0.7;">We specialize in residential moves, small or large, relocating across the country or across town! We move entire households, apartments, condominiums or even just a few items. Our attention to detail as well as the quality of our services we feel very confident that we will meet and or exceed your expectations. Moving Professionals 4U handles your entire job from beginning to end. You will find that we are not your average moving company, we not only conduct your move, but we provide intense moving management. We stand by our continuing commitment in providing our customers with an outstanding, stress free moving experience.</p>
                            </div>
                        </div>
                        <div class="col-md-4 pull-right real-state-img">
                            <img class="banner-img" width="450px" style="margin-left: -50px;" src="{{asset('images/real_state.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="overview-block-ptb grey-bg">
            <div class="container">
                <div class="row">
                    <div class="heading-title pull-left">
                        <h4 class="title iq-tw-7">Call us to speak with one of our moving managers to discuss the following:</h4>
                    </div>
                </div>
                <div class="row" style="margin-top: -100px;">
                    <div class="col-md-4">
                        <div class="single_work">
                            <a href=""><img class="pull-left" src="{{asset('images/home-services/man_icon.png')}}" alt=""></a>
                            <h4 class="pull-left">&nbsp;&nbsp;Experienced staff and movers</h4>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="pull-left" src="{{asset('images/home-services/man_icon.png')}}" alt=""></a>
                            <h4 class="pull-left">&nbsp;&nbsp;Employee Relocation packages</h4>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="pull-left" src="{{asset('images/home-services/track_two.png')}}" alt=""></a>
                            <h4 class="pull-left">&nbsp;&nbsp;Full & partial packing</h4>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="pull-left" src="{{asset('images/home-services/track_two.png')}}" alt=""></a>
                            <h4 class="pull-left">&nbsp;&nbsp;Full Value Replacement<br/>&nbsp; Insurance</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single_work">
                            <a href=""><img class="pull-left" src="{{asset('images/home-services/track_two.png')}}" alt=""></a>
                            <h4 class="pull-left">&nbsp;&nbsp;Complete assembly,<br/>&nbsp; disassembly and placement of<br/>&nbsp; furniture and boxes</h4>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="pull-left" src="{{asset('images/home-services/track_two.png')}}" alt=""></a>
                            <h4 class="pull-left">&nbsp;&nbsp;Upholstery shrink wrapping</h4>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="pull-left" src="{{asset('images/home-services/track_two.png')}}" alt=""></a>
                            <h4 class="pull-left">&nbsp;&nbsp;Specialty moving -<br/>&nbsp; pianos, safes, hot tubs,<br/>&nbsp; playground equipment, etc.</h4>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="pull-left" src="{{asset('images/home-services/track_two.png')}}" alt=""></a>
                            <h4 class="pull-left">&nbsp;&nbsp;Floor protection for<br/>&nbsp; carpet, tile, hardwood<br/>&nbsp; flooring, etc</h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="single_work">
                            <a href=""><img class="pull-left" src="{{asset('images/home-services/track_two.png')}}" alt=""></a>
                            <h4 class="pull-left">&nbsp;&nbsp;On truck storage to<br/>&nbsp; accommodate home closing<br/>&nbsp; dates</h4>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="pull-left" src="{{asset('images/home-services/track_two.png')}}" alt=""></a>
                            <h4 class="pull-left">&nbsp;&nbsp;Safe, expert handling<br/>&nbsp; of all your household<br/>&nbsp; goods</h4>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="pull-left" src="{{asset('images/home-services/track_two.png')}}" alt=""></a>
                            <h4 class="pull-left">&nbsp;&nbsp;Competitive pricing!</h4>
                        </div>
                    </div>
                </div><br/>
            </div>
        </section>
        <section id="how-it-works" class="iq-bg-over">
            <div class="container">
                <div class="row"></div><br/><br/>
                <div class="row">
                    <div class="col-md-4">
                        <div class="iq-works-box text-left">
                            <div class="icon-bg text-center">
                                <img src="images/services/icon1.png" class="img-fluid" alt="#">
                            </div>
                            <h5 class="iq-tw-7 text-uppercase iq-mt-25 iq-mb-15">Custom Packing</h5>
                            <p>We provide packing services for simply one specialty item, such as a television, mirror or wall hangings, or we can pack your entire household. Standard and specialty cartons are included in providing this professional care service.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="iq-works-box text-left">
                            <div class="icon-bg text-center">
                                <img src="images/services/icon2.png" class="img-fluid" alt="#">
                            </div>
                            <h5 class="iq-tw-7 text-uppercase iq-mt-25 iq-mb-15">Experienced Packers</h5>
                            <p>Our packing teams are experienced household packing professionals.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="iq-works-box text-left">
                            <div class="icon-bg text-center">
                                <img src="images/services/icon1.png" class="img-fluid" alt="#">
                            </div>
                            <h5 class="iq-tw-7 text-uppercase iq-mt-25 iq-mb-15">Bulky Item Moving</h5>
                            <p>We move pianos, safes, workshop tools, trampolines, yard equipment, etc. We have specialized equipment for the handling these items.</p>
                        </div>
                    </div>
                </div><br/>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <div class="iq-works-box text-left">
                            <div class="icon-bg text-center">
                                <img src="images/services/icon1.png" class="img-fluid" alt="#">
                            </div>
                            <h5 class="iq-tw-7 text-uppercase iq-mt-25 iq-mb-15">Pad Wrapped Service</h5>
                            <p>Our crews will take great care of your belongings by padding and wrapping each item. Your load will be secured with straps, and load bars when required.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="iq-works-box text-left">
                            <div class="icon-bg text-center">
                                <img src="images/services/icon2.png" class="img-fluid" alt="#">
                            </div>
                            <h5 class="iq-tw-7 text-uppercase iq-mt-25 iq-mb-15">Fragile Items / Antiques</h5>
                            <p>Packing delicate items requires reinforced cartons to provide protection. We offer specialty packing and crating for Fragile Items and also off Antique packaged as well.</p>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row"></div><br/><br/>
            </div>
        </section>
        <section class="overview-block-ptb iq-bg-over  iq-over-blue-50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="heading-title">
                            <h3 class="title iq-tw-7">Office & Commercial Moving</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left">
                            <p style="opacity: 0.7;font-size: 24px;margin-top: 20px;">We specialize in moving homes as well as businesses, whether it be a single office and or the relocating of your employees. Moving can be an incredibly time consuming process and the key to an efficient move is the organized fashion it which the move is performed. Moving Professionals 4U allows your staff to minimize downtime by having our moving managers plan a strategic and efficient move with the direction of your coordinators.</p>
                        </div>
                        <div class="col-md-6 pull-right">
                            <img class="banner-img office-inside" width="400px" src="{{asset('images/office_inside.jpg')}}" alt="">
                            <img class="banner-img office-outside" width="400px" src="{{asset('images/office_outside.jpg')}}" alt="">
                        </div>
                    </div>
                </div><br/><br/><br/>
            </div>
        </section>
        <section id="great-screenshots" class="overview-block-ptb iq-bg-over iq-screenshots iq-over-black-50 iq-parallax" data-jarallax='{"speed": 0.6}' style="background: url('images/time_background.jpg');margin-top: -50px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left">
                            <img class="clock-gif" width="200px" style="margin-left: 115px;" src="{{asset('images/clock.gif')}}" alt="">
                        </div>
                        <div class="col-md-6 pull-right">
                            <h3 style="margin-top: 30px;font-weight: 800;color: #FFFFFF;">We offer after hours<br/> moving to accommodate<br/> your schedule!</h3>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="overview-block-ptb iq-bg-over  iq-over-blue-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6 pull-left">
                            <h4>Specialized & Standard Packing Supplies</h4>
                            <ol class="packing-supplies" type="1" style="margin-left: 120px;">
                                <li>4.5 cubic feet</li>
                                <li>Dish Pack - 5 cubic feet</li>
                                <li>Mirror Pack</li>
                                <li>File box</li>
                                <li>Tape</li>
                                <li>Washer Lock</li>
                                <li>Packing paper</li>
                                <li>Dish dividers</li>
                                <li>Shrink wrap</li>
                                <li>Bubble wrap</li>
                                <li>6.0 cubic feet Large Box</li>
                                <li>3.0 cubic feet Medium Box</li>
                                <li>1.5 cubic feet Small Box</li>
                                <li>Wardrobe Box</li>
                            </ol>
                        </div>
                        <div class="col-md-6 pull-right">
                            <img class="banner-img home-services2" width="500px" style="margin-top: 35px;" src="{{asset('images/service_goods.png')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="how-works" class="overview-block-ptb how-works">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="heading-title">
                            <h3 class="title iq-tw-7">Senior Relocation</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-6 pull-left">
                            <p style="opacity: 0.7;margin-top: 70px;font-size: 16px;">No matter if your elderly loved one is moving long distance or locally, you can trust our senior moving team to provide the customized moving solutions required to deliver a stress free senior relocation. You will work directly with our kind and knowledgeable staff to make informed decisions that best fit your seniors needs and budget. With our comprehensive list of a la carte options and concierge services, Moving Professionals 4U has the utmost confidence in providing you an organized and hassle free senior move.</p>
                        </div>
                        <div class="col-md-6 pull-right">
                            <img class="banner-img home-services3" width="500px" src="{{asset('images/elderly_couple.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="iq-objects">
                <span class="iq-objects-01">
                        <img src="images/drive/02.png" alt="drive02">
                    </span>
                <span class="iq-objects-02" data-bottom="transform:translatey(50px)" data-top="transform:translatey(-100px);">
                        <img src="images/drive/03.png" alt="drive02">
                    </span>
                <span class="iq-objects-03" data-bottom="transform:translatex(50px)" data-top="transform:translatex(-100px);">
                        <img src="images/drive/04.png" alt="drive02">
                    </span>
                <span class="iq-objects-04 iq-fadebounce">
                        <span class="iq-round"></span>
                </span>
            </div>
        </section>
        <section class="overview-block-ptb grey-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-4 col-md-6 r4-mt-30">
                        <div class="iq-fancy-box text-center">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-people-outline"></i>
                            </div>
                            <div class="fancy-content">
                                <h5 class="iq-tw-6 iq-pt-20 iq-pb-10">Trained & Uniformed Professionals</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-4 col-md-6 r4-mt-30">
                        <div class="iq-fancy-box text-center">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-checkmark-outline"></i>
                            </div>
                            <div class="fancy-content">
                                <h5 class="iq-tw-6 iq-pt-20 iq-pb-10">Fully Licensed & Insured</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-4 col-md-6 r4-mt-30">
                        <div class="iq-fancy-box text-center">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-cash"></i>
                            </div>
                            <div class="fancy-content">
                                <h5 class="iq-tw-6 iq-pt-20 iq-pb-10">No Hidden Fees or Charges AT ALL</h5>
                            </div>
                        </div>
                    </div>
                </div><br/><br/>
                <div class="row">
                    <div class="col-sm-12 col-lg-4 col-md-6 r4-mt-30">
                        <div class="iq-fancy-box text-center">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-cart-outline"></i>
                            </div>
                            <div class="fancy-content">
                                <h5 class="iq-tw-6 iq-pt-20 iq-pb-10">Packing & Unpacking Services upon request</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-4 col-md-6 r4-mt-30">
                        <div class="iq-fancy-box text-center">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-time-outline"></i>
                            </div>
                            <div class="fancy-content">
                                <h5 class="iq-tw-6 iq-pt-20 iq-pb-10">Flexible Scheduling (7 days a week / 24 hours a day)</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-4 col-md-6 r4-mt-30">
                        <div class="iq-fancy-box text-center">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-paperplane-outline"></i>
                            </div>
                            <div class="fancy-content">
                                <h5 class="iq-tw-6 iq-pt-20 iq-pb-10">Moving Management Services</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    {{--<script>
        $(window).scroll(function(){
            $('.res-move').each( function(i){
                var bottom_of_object = $(this).position().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();
                if( bottom_of_window > bottom_of_object ) {
                    $('.real-state-img').fadeIn(3000);
                }
            });
        });
    </script>--}}
@endsection