@extends("master")

@section("content")
    <!-- Header -->
    <header id="main-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{url('/')}}">
                            <img class="img-fluid" src="{{ asset('images/logo.png') }}" alt="#">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="ion-navicon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto w-100 justify-content-end">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/')}}">Home</a>
                                </li>
                                {{--<li class="nav-item">
                                    <a class="nav-link" href="{{url('/services')}}">Services</a>
                                </li>--}}
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/about-us')}}">About Us</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link active" href="{{url('/contact-us')}}">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->
    <div class="main-content">
        <section id="how-it-works" class="overview-block-ptb it-works iq-over-blue-40" style="margin-top: 30px;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="heading-title">
                            <h3 class="title iq-tw-7">IF YOU HAVE QUESTIONS CONTACT US!</h3>
                            <p>Never heard the word impossible. This time there's no stopping us. These Happy Days are yours
                                and mine Happy Days. Today still wanted by the government they survive as soldiers of fortune.
                                Said Californ'y is the place you ought to be So they loaded up the truck and moved to Beverly.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <div class="iq-footer-box text-left">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-location-outline"></i>
                            </div>
                            <div class="footer-content">
                                <h4 class="iq-tw-6 iq-pb-10">Address</h4>
                                <p>1234 North Avenue Luke Lane, South Bend, IN 360001</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 r4-mt-30">
                        <div class="iq-footer-box text-left">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-telephone-outline"></i>
                            </div>
                            <div class="footer-content">
                                <h4 class="iq-tw-6 iq-pb-10">Phone</h4>
                                <p>+0123 456 789
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 r-mt-30">
                        <div class="iq-footer-box text-left">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-email-outline"></i>
                            </div>
                            <div class="footer-content">
                                <h4 class="iq-tw-6 iq-pb-10">Mail</h4>
                                <p>support@sofbox.com
                                    <br>24 X 7 online support
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 r-mt-30">
                        <div class="iq-footer-box text-left">
                            <div class="iq-icon">
                                <i aria-hidden="true" class="ion-ios-clock-outline"></i>
                            </div>
                            <div class="footer-content">
                                <h4 class="iq-tw-6 iq-pb-10">
                                    Working Hours</h4>
                                <p>Monday To Friday : 9:00 Am to 9:00 Pm
                                    <br>Saturday : 9:00 Am to 5:00 Pm
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="map-info white-bg">
            <div class="container">
                <div class="row">
                    <div class="contact-map">
                        <div id="googleMap" style="width:100%;height:600px;"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.840108181602!2d144.95373631539215!3d-37.8172139797516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1497005461921" frameborder="0" allowfullscreen
                                                                                     style="width:1390px;height:600px;padding-top:60px;margin-left:-90px"></iframe></div>
                    </div>
                </div>
            </div>
        </section>
        <section id="how-it-works" class="overview-block-ptb it-works white-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="heading-title">
                            <h3 class="title iq-tw-7">GET IN TOUCH WITH US!</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form id="contact" method="post">
                            <div class="contact-form">
                                <div class="section-field">
                                    <input class="require" id="contact_name" type="text" placeholder="Name*" name="name">
                                </div>
                                <div class="section-field">
                                    <input class="require" id="contact_email" type="email" placeholder="Email*" name="email">
                                </div>
                                <div class="section-field">
                                    <input class="require" id="contact_phone" type="text" placeholder="Phone*" name="phone">
                                </div>
                                <div class="section-field textarea">
                                    <textarea id="contact_message" class="input-message require" placeholder="Comment*" rows="5" name="message"></textarea>
                                </div>
                                <button id="submit" name="submit" type="submit" value="Send" class="button iq-mt-15">Send Message</button>
                                <div class="alert alert-success alert-dismissible fade show" role="alert" id="success">
                                    <strong>Thank You, Your message has been received.</strong>.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </section>
    </div>

@endsection